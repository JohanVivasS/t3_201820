package model.data_structures;

import java.util.Iterator;


public class DoubleLinkedList <T> implements IDoublyLinkedList<T>{

	private Node primerNodo;

	private Node ultimoNodo;

	private int sizeLista;

	public DoubleLinkedList(){
		primerNodo = null;
		ultimoNodo=null;
		sizeLista = 0;
	}

	public DoubleLinkedList(T info){
		primerNodo = new Node<T>(info);
		ultimoNodo= primerNodo;
		sizeLista = 1;
	}

	public int getSize(){
		return sizeLista;
	}

	public boolean isEmpty(){

		return sizeLista == 0;
	}

	public boolean agregar(T elementoLista){
		if(primerNodo==null)
		{
			primerNodo= new Node<T>(elementoLista);
			ultimoNodo = primerNodo;
			sizeLista++;
			return true;
		}else
		{
			sizeLista++;
			Node<T> nuevo = new Node<T>(elementoLista);
			ultimoNodo.setNext(nuevo);
			nuevo.setPrevious(ultimoNodo);
			ultimoNodo = nuevo;
			return true;
		}
	}

	public void eliminar(T elementLista){
		
		if(ultimoNodo.getInfo().equals(elementLista))
		{
			ultimoNodo=ultimoNodo.previous;
			ultimoNodo.next = null;
		}
		else if( primerNodo.getInfo().equals(elementLista))
		{
			primerNodo = primerNodo.next;
			primerNodo.previous = null;
		}
		
		if(primerNodo != null)
		{	
			Node<T> actual = primerNodo;
			if(actual.hasNext())
			{
				Node<T> sig = actual.next;
				if(sig.getInfo().equals(elementLista))
				{
					actual.next=sig.next;
					sig.next.previous=actual;
				}
				else{
					actual=actual.next;
				}
			}
						
		}
	}
	
	
	public class Node<T> {

		private T info;
		private Node<T> next;
		private Node<T> previous;

		public Node(T info){
			this.info=info;
			next=null;
			previous=null;
		}
		public T getInfo() {
			return info;
		}
		public void setInfo(T info) {
			this.info = info;
		}
		public Node<T> getNext() {
			return next;
		}
		public void setNext(Node<T> next) {
			this.next = next;
		}
		public Node<T> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<T> previous) {
			this.previous = previous;
		}

		public boolean hasNext(){
			return next!=null;
		}

		public boolean hasPrevious(){
			return next!=null;
		}


	}


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}


}
