package model.data_structures;

import java.util.Iterator;
import java.util.LinkedList;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T>{

	private LinkedList lista = new LinkedList();
	
	private Nodo<T> actual = null;
	
	private Nodo<T> primero;
	
	int pos =0;
	
	private int size;
	
	public DoublyLinkedList() {
		primero=null;
		size=0;
	}

	
	@Override
	public int getSize() {
		return size;
	}
	

	@Override
	public Iterator<T> iterator(){	
		return  new ListaIterator();
		}
	
	 private class ListaIterator implements Iterator<T> {
		 
		 public Nodo<T> ref;
		 
		 public ListaIterator(){
			 if(primero!=null)
				 ref=primero;
		 }
		 
		 public boolean hasNext() { 
			 return pos<size;
			 }
		 public T next()
		 {
			 T item = null;
			 if(hasNext()){
				 try {
					 item = ref.getItem();
					 ref = ref.darSiguiente();
					 pos++;
				} catch (Exception e) {
					// TODO: handle exception
				}		
			 }
			 return item;
		 }
	 } 
	public void agregar(T elemento){
		if(primero==null){
			primero=new Nodo<T>(elemento,actual);
			actual=primero;
			size=1;
		}
		else{
			actual.cambiarSiguiente(new Nodo<T>(elemento,actual));
			size++;
		}
	}
	
	public int hashCode(){
		int res = 0;
		for(Nodo<T> aux = primero ; aux != null ; aux = aux.darSiguiente())
			res = aux.getItem().hashCode();
		return res;
	}
	
	public String toString(){
		String res="";
		for(Nodo<T> aux = primero ; aux != null ; aux = aux.darSiguiente())
			res+=aux.getItem().toString()+"\n";
		return res;
	}
//	public T get(int n)
//	{
//		T itemI = null;
//		int cont = 0;
//		Nodo<T> temp = null;
//		
//		for(int i=0;i<size;i++){
//			if (primero != null && cont == 0){
//				temp = primero;
//				cont = 1;
//				System.out.println(cont+"");
//				}
//			else if (primero != null && cont > 0 && ){
//				temp = temp.darSiguiente();
//				cont++;
//				}
//			if(cont==n){
//				itemI = temp.getItem();
//				}
//		}
//		return itemI;
//	}
}
//	public void agregar(String pId,String pName,String pCity, String pLatitude, String pLongitude, String pDpcapacity, String pOnline_date){
//		int id=Integer.parseInt(pId);
//		double latitud=Double.parseDouble(pLatitude);
//		double longitud=Double.parseDouble(pLongitude);
//		int dpCapacity = Integer.parseInt(pDpcapacity);
//		if(primero==null){
//			primero = new Nodo(id,pName,pCity, latitud,longitud,dpCapacity,pOnline_date,null);
//			actual = primero;
//			referencia = primero;
//		}
//		else{
//			referencia.cambiarSiguiente(new Nodo(id,pName,pCity, latitud,longitud,dpCapacity,pOnline_date,referencia));
//			referencia=referencia.darSiguiente();
//		}}

